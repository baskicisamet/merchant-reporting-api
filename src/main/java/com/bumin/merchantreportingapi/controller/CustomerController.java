package com.bumin.merchantreportingapi.controller;

import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bumin.merchantreportingapi.dto.CustomerInfoDTOWrapper;
import com.bumin.merchantreportingapi.service.CustomerService;
import com.bumin.merchantreportingapi.util.UUIDUtil;

@RestController
@RequestMapping("api/v3/client")
public class CustomerController {

	private CustomerService customerService;

	@Autowired
	public CustomerController(CustomerService customerService) {
		super();
		this.customerService = customerService;
	}

	@PostMapping
	public CustomerInfoDTOWrapper getCustomer(@RequestBody Object transactionId) {

		String strId = (String) ((Map) transactionId).get("transactionId");
		UUID id = UUIDUtil.fromString(strId);

		return customerService.getByTransactionId(id);

	}

}
