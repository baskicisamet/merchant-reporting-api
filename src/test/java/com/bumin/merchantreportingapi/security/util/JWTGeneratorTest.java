package com.bumin.merchantreportingapi.security.util;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import com.bumin.merchantreportingapi.security.model.JWTUserDetails;

public class JWTGeneratorTest {
	
	JWTGenerator jwtGenerator;
	
	 @Before
	    public void setUp() throws Exception {
	        jwtGenerator = new JWTGenerator();
	    }
	 
	 
	 @Test
	 public void generateTest() throws Exception {
		 
		 //GIVEN
		 JWTUserDetails jwtUserDetails = new JWTUserDetails(1l, "xxx@xxx.com", "qqq", true, null);
		 //WHEN
		 String token = jwtGenerator.generate(jwtUserDetails);
		 //THEN
		 assertNotNull("Token is null", token);
		 
	 }

}
