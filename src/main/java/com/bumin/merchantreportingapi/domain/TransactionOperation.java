package com.bumin.merchantreportingapi.domain;

public enum TransactionOperation {
	
	DIRECT,
	REFUND,
	_3D{
	      public String toString() {
	          return "3D";
	      }   
	}
	,
	_3DAUTH{
		 public String toString() {
	          return "3DAUTH";
	      }   
	},
	STORED
	
	//DIRECT, REFUND, A3D(""), 3DAUTH, STORED
}
