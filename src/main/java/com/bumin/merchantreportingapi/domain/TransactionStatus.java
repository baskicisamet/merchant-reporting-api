package com.bumin.merchantreportingapi.domain;

public enum TransactionStatus {
	
	APPROVED, WAITING, DECLINED, ERROR 

}
