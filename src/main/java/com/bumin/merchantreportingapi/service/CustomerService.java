package com.bumin.merchantreportingapi.service;

import java.util.UUID;

import com.bumin.merchantreportingapi.dto.CustomerInfoDTOWrapper;



public interface CustomerService {
	
	CustomerInfoDTOWrapper getByTransactionId(UUID id);
	
}
