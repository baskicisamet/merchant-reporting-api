# Reporting API
...

### Request payloads for successfull response

  **http://localhost:8080/api/v3/merchant/user/login** : 
  
---
 
```json
{
	"email":"merchant1@merchant.com",
	"password":"password"
}
```
---

 **http://localhost:8080/api/v3/transaction/report** : 
 
 ---
 
```json
{
    "fromDate": "2015-07-01",
    "toDate": "2020-10-01",
    "merchant": 1,
    "acquirer": 1
}
```
---
  **http://localhost:8080/api/v3/transaction** :
 
---

```json
{
	"transactionId":"[copy from console or h2 console]"
}
```
---

 **http://localhost:8080/api/v3/client** :
 
---

```json
{
	"transactionId":"[copy from console or h2 console]"
}
```
---

To see  all initialized test data : http://localhost:8080/h2-console/




