package com.bumin.merchantreportingapi.dto.request;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TransactionReportRequestDTO {

	private Date fromDate;
	private Date toDate;
	private Long merchant;
	private Long acquirer;
}
