package com.bumin.merchantreportingapi.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;



import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"email"})
})
public class Merchant{
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Long id;
	
	@Column(length=128)
	private String name;
	
	@Column(length=128)
	private String email;
	
	
	private String password;
	
	private boolean enabled;
	
	@OneToMany( mappedBy = "merchant", cascade = CascadeType.REMOVE)
	private List<Transaction> transactions = new ArrayList<>();
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name ="merchant_role", joinColumns= @JoinColumn(name="merchant_id"), inverseJoinColumns= @JoinColumn(name="role_id"))	
	private List<Role> roles = new ArrayList<Role>();
	
	
	
	
	public Merchant addRole(Role role) {
		this.roles.add(role);
		return this;
	}

}
