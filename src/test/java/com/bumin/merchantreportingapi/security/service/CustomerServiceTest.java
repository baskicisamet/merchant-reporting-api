package com.bumin.merchantreportingapi.security.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.bumin.merchantreportingapi.converter.CustomerToCustomerInfoDTOConverter;
import com.bumin.merchantreportingapi.converter.TransactionToSingleTransactionWrapperDTOConverter;
import com.bumin.merchantreportingapi.domain.Currency;
import com.bumin.merchantreportingapi.domain.Customer;
import com.bumin.merchantreportingapi.dto.CustomerInfoDTO;
import com.bumin.merchantreportingapi.dto.CustomerInfoDTOWrapper;
import com.bumin.merchantreportingapi.dto.TransactionReportDTO;
import com.bumin.merchantreportingapi.repository.CustomerRepository;
import com.bumin.merchantreportingapi.repository.TransactionRepository;
import com.bumin.merchantreportingapi.service.CustomerService;
import com.bumin.merchantreportingapi.service.CustomerServiceImpl;
import com.bumin.merchantreportingapi.service.TransactionService;
import com.bumin.merchantreportingapi.service.TransactionServiceImpl;

public class CustomerServiceTest {
	
	CustomerService  customerService;

	@Mock
	CustomerRepository customerRepository;
	
	@Mock
	CustomerToCustomerInfoDTOConverter converter;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		customerService = new CustomerServiceImpl(customerRepository,converter);
	}
	
	
	@Test
	public void getByTransactionId() throws Exception {
		
		
		//given
		Optional<Customer> customerOptional = Optional.of(new Customer());
		CustomerInfoDTO customerInfoDTO = CustomerInfoDTO.builder().build();

		when(customerRepository.findByTransactionsId(any())).thenReturn(customerOptional);
		when(converter.convert(any())).thenReturn(customerInfoDTO);
		 
      
		
		//when
		CustomerInfoDTOWrapper resultCustomer = customerService.getByTransactionId(any());
		
		//then
		assertNotNull(resultCustomer);
		verify(customerRepository, times(1)).findByTransactionsId(any());
		verify(converter, times(1)).convert(any());
		
		
	}

}
