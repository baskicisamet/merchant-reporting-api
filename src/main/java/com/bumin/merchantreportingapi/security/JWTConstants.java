package com.bumin.merchantreportingapi.security;

import io.jsonwebtoken.SignatureAlgorithm;

public class JWTConstants {
	
	public static final String  SECRET = "secretkey";
	public static final String  HEADER = "Authorization";
	public static final long EXPIRATION_TIME = 600_000L;
	public static final SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS512;
}
