package com.bumin.merchantreportingapi.security.model;

public enum RequestStatus {
	
	APPROVED, WAITING, DECLINED, ERROR

}
