package com.bumin.merchantreportingapi.security.util;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.bumin.merchantreportingapi.security.JWTConstants;
import com.bumin.merchantreportingapi.security.model.JWTUserDetails;

import io.jsonwebtoken.Jwts;

@Component
public class JWTGenerator {
	
	
public String generate(JWTUserDetails user) {// this user is authentication result
		
		return Jwts.builder()
				.setSubject(user.getEmail())
				.setIssuedAt(new Date(System.currentTimeMillis() ))
				.setExpiration(new Date(System.currentTimeMillis() + JWTConstants.EXPIRATION_TIME))
				.signWith(JWTConstants.SIGNATURE_ALGORITHM, JWTConstants.SECRET)
				.compact();
	}
	
	
}
