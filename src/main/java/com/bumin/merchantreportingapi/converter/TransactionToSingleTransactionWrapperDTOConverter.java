package com.bumin.merchantreportingapi.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import com.bumin.merchantreportingapi.domain.Transaction;
import com.bumin.merchantreportingapi.dto.CustomerInfoDTO;
import com.bumin.merchantreportingapi.dto.FXDTO;
import com.bumin.merchantreportingapi.dto.SingleTransactionDTO;
import com.bumin.merchantreportingapi.dto.SingleTransactionMerchantDTO;
import com.bumin.merchantreportingapi.dto.SingleTransactionMerchantInfoDTO;
import com.bumin.merchantreportingapi.dto.SingleTransactionWrapperDTO;

import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class TransactionToSingleTransactionWrapperDTOConverter implements Converter<Transaction, SingleTransactionWrapperDTO> {
	
	
	private FXToFXMerchantDTOConverter fxToFXMerchantDTOConverter;
	private CustomerToCustomerInfoDTOConverter customerToCustomerInfoDTOConverter;
	private TransactionToSingleTransactionMerchantDTOConverter transactionToSingleTransactionMerchantDTOConverter;
	
	
	@Autowired
	public TransactionToSingleTransactionWrapperDTOConverter(
			FXToFXMerchantDTOConverter fxToFXMerchantDTOConverter,
			CustomerToCustomerInfoDTOConverter customerToCustomerInfoDTOConverter,
			TransactionToSingleTransactionMerchantDTOConverter transactionToSingleTransactionMerchantDTOConverter
	){
		
		this.fxToFXMerchantDTOConverter = fxToFXMerchantDTOConverter;
		this.customerToCustomerInfoDTOConverter = customerToCustomerInfoDTOConverter;
		this.transactionToSingleTransactionMerchantDTOConverter = transactionToSingleTransactionMerchantDTOConverter;
		
	}

	@Synchronized
	@Nullable
	@Override
	public SingleTransactionWrapperDTO convert(Transaction source) {
		
		if(source == null) {
			log.error("source is null");
			return null;
		}
		
		SingleTransactionWrapperDTO target = new SingleTransactionWrapperDTO();
		
		//creating root fields
		FXDTO fxdto = new FXDTO(fxToFXMerchantDTOConverter.convert(source.getMerchantTransaction().getFx()));
		
		CustomerInfoDTO customerInfoDTO  = customerToCustomerInfoDTOConverter.convert(source.getCustomer());
		
		SingleTransactionMerchantInfoDTO singleTransactionMerchantInfoDTO = new SingleTransactionMerchantInfoDTO(source.getMerchant().getName());
		
		SingleTransactionMerchantDTO singleTransactionMerchantDTO = transactionToSingleTransactionMerchantDTOConverter.convert(source);
		
		SingleTransactionDTO singleTransactionDTO = new SingleTransactionDTO(singleTransactionMerchantDTO);
		
		target.setFx(fxdto);
		target.setCustomerInfo(customerInfoDTO);
		target.setMerchant(singleTransactionMerchantInfoDTO);
		target.setTransaction(singleTransactionDTO);
		
		return target;
	}




	

}
