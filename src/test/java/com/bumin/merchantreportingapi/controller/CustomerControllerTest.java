package com.bumin.merchantreportingapi.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.bumin.merchantreportingapi.dto.CustomerInfoDTO;
import com.bumin.merchantreportingapi.dto.CustomerInfoDTOWrapper;
import com.bumin.merchantreportingapi.exception.CustomerNotFoundException;
import com.bumin.merchantreportingapi.service.CustomerService;

public class CustomerControllerTest {

	@Mock
	CustomerService customerService;

	@InjectMocks
	CustomerController customerController;

	MockMvc mockMvc;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		mockMvc = MockMvcBuilders
				.standaloneSetup(customerController)
				.setControllerAdvice(new RestResponseEntityExceptionHandler())
				.build();
	}
	
	 
	@Test
	public void getCustomer() throws Exception {
		
		//given
		String email = "test@test.com";
		CustomerInfoDTO dto = CustomerInfoDTO.builder().email(email).build();
		CustomerInfoDTOWrapper dtoWrapper = new CustomerInfoDTOWrapper(dto);
		
		String requestString = "{\"transactionId\":\"84decbdf5e964485ac1da6eca7b4fcee\"}";
		
		when(customerService.getByTransactionId(any())).thenReturn(dtoWrapper);
		
		//when 
		mockMvc.perform(post("/api/v3/client")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestString))
		//then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.customerInfo.email", equalTo(email)));
		
		
	}
	
	 @Test
    public void testCustomerNotFound() throws Exception {

		 //given
		 String requestString = "{\"transactionId\":\"84decbdf5e964485ac1da6eca7b4fcee\"}";
        when(customerService.getByTransactionId(any())).thenThrow(CustomerNotFoundException.class);
        
        //when
        mockMvc.perform(post("/api/v3/client")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestString))
        //then
                .andExpect(status().isNotFound());
    }

	
}
