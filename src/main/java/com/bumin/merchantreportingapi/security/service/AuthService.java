package com.bumin.merchantreportingapi.security.service;

import com.bumin.merchantreportingapi.security.model.JWTAuthenticationResonse;
import com.bumin.merchantreportingapi.security.model.LoginRequest;

public interface AuthService {
	
	JWTAuthenticationResonse authenticate(LoginRequest loginRequest);

}
