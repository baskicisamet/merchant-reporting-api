package com.bumin.merchantreportingapi.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class SingleTransactionMerchantDTO {
	
	private String referenceNo;
	private Long merchantId;
	private String status;
	private String channel;
	private String customData;
	private String chainId;
	private Long agentInfoId;
	private String operation;
	private Long fxTransactionId;
	@JsonProperty("updated_at")
	private Date updatedAt;
	@JsonProperty("created_at")
	private Date createdAt;
	private Long id;
	private Long acquirerTransactionId;
	private String code;
	private String message;
	private String transactionId;

}
