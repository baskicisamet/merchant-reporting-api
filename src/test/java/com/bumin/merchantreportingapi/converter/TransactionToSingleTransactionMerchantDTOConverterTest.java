package com.bumin.merchantreportingapi.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import com.bumin.merchantreportingapi.domain.AcquirerTransaction;
import com.bumin.merchantreportingapi.domain.FX;
import com.bumin.merchantreportingapi.domain.Merchant;
import com.bumin.merchantreportingapi.domain.MerchantTransaction;
import com.bumin.merchantreportingapi.domain.Transaction;
import com.bumin.merchantreportingapi.dto.SingleTransactionMerchantDTO;

public class TransactionToSingleTransactionMerchantDTOConverterTest {
	

    public static final String  MESSAGE = "test message";
    public static final Long  ID = 1l;
    
   
    
    TransactionToSingleTransactionMerchantDTOConverter converter;
    
    @Before
    public void setUp() throws Exception {
    	converter = new TransactionToSingleTransactionMerchantDTOConverter(); 
    	
    }
    
    
    @Test
    public void testNullObject() throws Exception {
        assertNull(converter.convert(null));
    }
    
    @Test
    public void convert() throws Exception {
    	
        //given
    	Transaction transaction = new Transaction();
    	MerchantTransaction mTransaction = new MerchantTransaction();
    	mTransaction.setId(ID);
    	mTransaction.setMessage(MESSAGE);
    	mTransaction.setFx(new FX());
    	transaction.setMerchantTransaction(mTransaction);
    	transaction.setMerchant(new Merchant());
    	transaction.setAcquirerTransaction(new AcquirerTransaction());
    	
        
        //when
        SingleTransactionMerchantDTO dto = converter.convert(transaction);

        //then
        
        assertEquals(MESSAGE, dto.getMessage());
        assertEquals(ID, dto.getId());
        
        

    }

}
