package com.bumin.merchantreportingapi.service;

import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bumin.merchantreportingapi.converter.CustomerToCustomerInfoDTOConverter;
import com.bumin.merchantreportingapi.domain.Customer;
import com.bumin.merchantreportingapi.dto.CustomerInfoDTO;
import com.bumin.merchantreportingapi.dto.CustomerInfoDTOWrapper;
import com.bumin.merchantreportingapi.exception.CustomerNotFoundException;
import com.bumin.merchantreportingapi.repository.CustomerRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CustomerServiceImpl implements CustomerService {

	private CustomerRepository customerRepository;
	private CustomerToCustomerInfoDTOConverter converter;
	
	

	@Autowired
	public CustomerServiceImpl(CustomerRepository customerRepository, CustomerToCustomerInfoDTOConverter converter) {
		super();
		this.customerRepository = customerRepository;
		this.converter = converter;
	}



	@Override
	public CustomerInfoDTOWrapper getByTransactionId(UUID id) {
		
		if(id == null) {
			log.error("id connot be null");
		}
		
		Optional<Customer> customer = customerRepository.findByTransactionsId(id);
		if(!customer.isPresent()) {
			log.error("There is no customer with this transaction id");
			throw new CustomerNotFoundException("There is no customer with this transaction id");
		}
		
		CustomerInfoDTO dto = converter.convert(customer.get());
		return new CustomerInfoDTOWrapper(dto);
	}

}
