package com.bumin.merchantreportingapi.security.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class JWTAuthenticationResonse {
	
	private String token;
	private RequestStatus status;

}
