package com.bumin.merchantreportingapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bumin.merchantreportingapi.domain.Acquirer;

public interface AcquirerRepository extends JpaRepository<Acquirer, Long> {

	
}
