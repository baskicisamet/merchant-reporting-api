package com.bumin.merchantreportingapi.service;

import java.util.UUID;

import com.bumin.merchantreportingapi.dto.SingleTransactionWrapperDTO;
import com.bumin.merchantreportingapi.dto.TransactionReportsWrapperDTO;
import com.bumin.merchantreportingapi.dto.request.TransactionReportRequestDTO;

public interface TransactionService {
	
	TransactionReportsWrapperDTO getReports(TransactionReportRequestDTO transactionReportRequestDTO);
	
	SingleTransactionWrapperDTO  getById(UUID id);

}
