package com.bumin.merchantreportingapi.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bumin.merchantreportingapi.domain.Merchant;

public interface MerchantRepository extends JpaRepository<Merchant, Long>{
	
	Optional<Merchant> findByEmail(String email);

}
