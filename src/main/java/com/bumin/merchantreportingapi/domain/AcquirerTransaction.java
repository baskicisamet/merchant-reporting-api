package com.bumin.merchantreportingapi.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class AcquirerTransaction {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@OneToOne(mappedBy = "acquirerTransaction")
	private Transaction transaction;

	
	
	

}
