package com.bumin.merchantreportingapi.controller;

import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bumin.merchantreportingapi.dto.SingleTransactionWrapperDTO;
import com.bumin.merchantreportingapi.dto.TransactionReportsWrapperDTO;
import com.bumin.merchantreportingapi.dto.request.TransactionReportRequestDTO;
import com.bumin.merchantreportingapi.service.TransactionService;
import com.bumin.merchantreportingapi.util.UUIDUtil;

@RestController
@RequestMapping("api/v3/transaction")
public class TransactionController {
	
	private TransactionService transactionService;
	

	@Autowired
	public TransactionController(TransactionService transactionService) {
		super();
		this.transactionService = transactionService;
	}


	@PostMapping("/report")
	public TransactionReportsWrapperDTO getTransactionReportDTO(@RequestBody TransactionReportRequestDTO transactionReportRequestDTO) {
		
		return transactionService.getReports(transactionReportRequestDTO);
	}
	
	
	@PostMapping
	public SingleTransactionWrapperDTO getTransaction(@RequestBody Object transactionId) {
		
		String strId = (String)((Map)transactionId).get("transactionId");
		UUID id = UUIDUtil.fromString(strId);
		
		return transactionService.getById(id);

	}
	
	

}
