package com.bumin.merchantreportingapi.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import com.bumin.merchantreportingapi.domain.Transaction;
import com.bumin.merchantreportingapi.dto.SingleTransactionMerchantDTO;

import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class TransactionToSingleTransactionMerchantDTOConverter implements Converter<Transaction, SingleTransactionMerchantDTO> {
	
	
	@Synchronized
	@Nullable
	@Override
	public SingleTransactionMerchantDTO convert(Transaction source) {
		
		if(source == null) {
			log.error("source is null");
			return null;
		}
		
		
		return SingleTransactionMerchantDTO.builder()
				
				.referenceNo(source.getMerchantTransaction().getReferenceNo())
				.merchantId(source.getMerchant().getId())
				.status(toStringIfNotNull(source.getMerchantTransaction().getStatus()))
				.channel(source.getMerchantTransaction().getChannel())
				.customData(source.getMerchantTransaction().getCustomData())
				.chainId(source.getMerchantTransaction().getChainId())
				.operation(toStringIfNotNull(source.getMerchantTransaction().getOperation()))
				.fxTransactionId(source.getMerchantTransaction().getFx().getId())
				.updatedAt(source.getMerchantTransaction().getUpdatedDate())
				.createdAt(source.getMerchantTransaction().getCreatedDate())
				.id(source.getMerchantTransaction().getId())
				.acquirerTransactionId(source.getAcquirerTransaction().getId())
				.code(source.getMerchantTransaction().getCode())
				.message(source.getMerchantTransaction().getMessage())
				.transactionId(toStringIfNotNull(source.getId()))
				
				.build();
		
	}	
	
	
	private String toStringIfNotNull(Object object) {
		if(object == null) {
			return null;
		}
		return object.toString();
	}
		

}
