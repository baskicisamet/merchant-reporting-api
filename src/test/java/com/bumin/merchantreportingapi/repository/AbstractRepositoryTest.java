package com.bumin.merchantreportingapi.repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import com.bumin.merchantreportingapi.domain.Acquirer;
import com.bumin.merchantreportingapi.domain.AcquirerTransaction;
import com.bumin.merchantreportingapi.domain.Currency;
import com.bumin.merchantreportingapi.domain.FX;
import com.bumin.merchantreportingapi.domain.Merchant;
import com.bumin.merchantreportingapi.domain.MerchantTransaction;
import com.bumin.merchantreportingapi.domain.Transaction;
import com.bumin.merchantreportingapi.domain.TransactionPaymentMethod;

public abstract class AbstractRepositoryTest {

	public TestEntityManager testEntityManager;
	

	
	
	
	public TestEntityManager getTestEntityManager() {
		return testEntityManager;
	}






	public void setTestEntityManager(TestEntityManager testEntityManager) {
		this.testEntityManager = testEntityManager;
	}





	public List<Transaction> loadTestData() {
		
		
		Merchant merchant = new Merchant();
		merchant.setEmail("test@test.com");
		merchant.setEnabled(true);
		merchant.setName("Merchant A");
		
		Acquirer acquirer = new Acquirer();
		acquirer.setName("Acquirer A");
		acquirer.setCode("XX");
		acquirer.setType(TransactionPaymentMethod.CREDITCARD);
		
		
		//transactions
		
		List<Transaction> transactions = new ArrayList<>();
		
		MerchantTransaction merchantTransaction = new MerchantTransaction();
		merchantTransaction.setCreatedDate(new Date());
		merchantTransaction.setFx(new FX(10.0, Currency.TRY));
		
		AcquirerTransaction acquirerTransaction = new AcquirerTransaction();
		
		Transaction transaction = new Transaction();
		transaction.setMerchant(merchant);
		transaction.setAcquirer(acquirer);
		transaction.setMerchantTransaction(merchantTransaction);
		transaction.setAcquirerTransaction(acquirerTransaction);
		merchantTransaction.setTransaction(transaction);
		acquirerTransaction.setTransaction(transaction);
		
		
		
		MerchantTransaction merchantTransaction2 = new MerchantTransaction();
		merchantTransaction2.setCreatedDate(new Date());
		merchantTransaction2.setFx(new FX(10.0, Currency.EUR));
		
		AcquirerTransaction acquirerTransaction2 = new AcquirerTransaction();
		
		Transaction transaction2 = new Transaction();
		transaction2.setMerchant(merchant);
		transaction2.setAcquirer(acquirer);
		transaction2.setMerchantTransaction(merchantTransaction2);
		transaction2.setAcquirerTransaction(acquirerTransaction2);
		merchantTransaction2.setTransaction(transaction2);
		acquirerTransaction2.setTransaction(transaction2);
		
		transactions.add(transaction);
		transactions.add(transaction2);
		
		acquirer.setTransactions(transactions);
		merchant.setTransactions(transactions);
		
		//save
		testEntityManager.persist(acquirer);
		testEntityManager.persist(merchant);
		testEntityManager.persist(transaction);
		testEntityManager.persist(transaction2);
		testEntityManager.flush();
		
		return transactions;
		
	}
	
	public Date getDate(String s) throws ParseException   {
		
		 SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		 return formatter.parse(s);
		 
	
	}
	
	
}
