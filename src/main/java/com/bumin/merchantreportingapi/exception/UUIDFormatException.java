package com.bumin.merchantreportingapi.exception;

public class UUIDFormatException extends RuntimeException {
	

	public UUIDFormatException() {
		
	}
	
	public UUIDFormatException(String mesage) {
		super(mesage);
	}
	
	public UUIDFormatException(String message,Throwable cause) {
		super(message,cause);
	}
	
	public UUIDFormatException(Throwable cause) {
		super(cause);
	}
	
	public UUIDFormatException(String message,Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message,cause,enableSuppression,writableStackTrace);
	}


}
