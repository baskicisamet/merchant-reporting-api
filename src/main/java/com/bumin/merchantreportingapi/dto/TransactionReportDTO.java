package com.bumin.merchantreportingapi.dto;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.bumin.merchantreportingapi.domain.Currency;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class TransactionReportDTO {

	private Long count;
	private double total;
	@Enumerated(EnumType.STRING)
	private Currency currency;
}
