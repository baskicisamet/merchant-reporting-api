package com.bumin.merchantreportingapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bumin.merchantreportingapi.domain.TransactionErrorCode;

public interface TransactionErrorCodeRepository extends JpaRepository<TransactionErrorCode, Long> {
	
}
