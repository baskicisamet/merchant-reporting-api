package com.bumin.merchantreportingapi.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bumin.merchantreportingapi.converter.TransactionToSingleTransactionWrapperDTOConverter;
import com.bumin.merchantreportingapi.domain.Transaction;
import com.bumin.merchantreportingapi.domain.TransactionStatus;
import com.bumin.merchantreportingapi.dto.SingleTransactionWrapperDTO;
import com.bumin.merchantreportingapi.dto.TransactionReportDTO;
import com.bumin.merchantreportingapi.dto.TransactionReportsWrapperDTO;
import com.bumin.merchantreportingapi.dto.request.TransactionReportRequestDTO;
import com.bumin.merchantreportingapi.exception.TransactionNotFoundException;
import com.bumin.merchantreportingapi.exception.TransactionReportEmptyContentException;
import com.bumin.merchantreportingapi.repository.TransactionRepository;

import ch.qos.logback.core.helpers.ThrowableToStringArray;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TransactionServiceImpl implements TransactionService {

	
	private TransactionRepository transactionRepository;
	private TransactionToSingleTransactionWrapperDTOConverter converter;
	
	
	@Autowired
	public TransactionServiceImpl(TransactionRepository transactionRepository,
			TransactionToSingleTransactionWrapperDTOConverter converter) {
		super();
		this.transactionRepository = transactionRepository;
		this.converter = converter;
	}



	@Override
	public TransactionReportsWrapperDTO getReports(TransactionReportRequestDTO transactionReportRequestDTO) {
		
		List<TransactionReportDTO> reports = transactionRepository.getTransactionReport(transactionReportRequestDTO);
		
		if(reports.isEmpty()) {
			log.warn("There is no report content");
			throw new TransactionReportEmptyContentException("There is no report content");
		}
		
		return new TransactionReportsWrapperDTO(TransactionStatus.APPROVED, reports);
	}
	
	
	
	@Override
	public SingleTransactionWrapperDTO getById(UUID id) {
		
		Optional<Transaction> transaction = transactionRepository.findById(id);
		
		if(!transaction.isPresent()) {
			log.error("transaction not found with this UUID");
			throw new TransactionNotFoundException("transaction not found with this UUID");
		}
		
		log.info("Loaing transaction with UUID");
		return converter.convert(transaction.get());
	}

	
	
	
}
