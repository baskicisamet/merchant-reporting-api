package com.bumin.merchantreportingapi.exception;


public class TransactionReportNotFoundException extends RuntimeException {
	

	public TransactionReportNotFoundException() {
		
	}
	
	public TransactionReportNotFoundException(String mesage) {
		super(mesage);
	}
	
	public TransactionReportNotFoundException(String message,Throwable cause) {
		super(message,cause);
	}
	
	public TransactionReportNotFoundException(Throwable cause) {
		super(cause);
	}
	
	public TransactionReportNotFoundException(String message,Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message,cause,enableSuppression,writableStackTrace);
	}


}
