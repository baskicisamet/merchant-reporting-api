package com.bumin.merchantreportingapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bumin.merchantreportingapi.domain.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

	
	
}
