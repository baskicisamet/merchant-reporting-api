package com.bumin.merchantreportingapi.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.bumin.merchantreportingapi.domain.Currency;
import com.bumin.merchantreportingapi.domain.Transaction;
import com.bumin.merchantreportingapi.dto.CustomerInfoDTO;
import com.bumin.merchantreportingapi.dto.FXMerchantDTO;
import com.bumin.merchantreportingapi.dto.SingleTransactionMerchantDTO;
import com.bumin.merchantreportingapi.dto.SingleTransactionWrapperDTO;

public class TransactionToSingleTransactionWrapperDTOConverterTest {
	
	
	 public static final String CHANNEL = "API";
	 public static final String CUSTOMER_EMAIL = "customer@customer.com";
	 public static final Currency CURRENCY = Currency.EUR;
	 public static final String  MESSAGE = "test message";
	 
	 @Mock
	 FXToFXMerchantDTOConverter fxToFXMerchantDTOConverter;
	 
	 @Mock
	 CustomerToCustomerInfoDTOConverter customerInfoDTOConverter;
	 
	 @Mock
	 TransactionToSingleTransactionMerchantDTOConverter transactionMerchantDTOConverter;
	    
	 TransactionToSingleTransactionWrapperDTOConverter converter;
	    
	 
		@Before
		public void setUp() throws Exception {
			
			MockitoAnnotations.initMocks(this);
			
			converter = new TransactionToSingleTransactionWrapperDTOConverter(fxToFXMerchantDTOConverter, 
					customerInfoDTOConverter,
					transactionMerchantDTOConverter);
			
		}
	    
	    
	    @Test
	    public void testNullObject() throws Exception {
	        assertNull(converter.convert(null));
	    }
	    
	    
	    @Ignore //TODO the problem is mock objects are not injected
	    @Test
	    public void convert() throws Exception {
	    	
	        //given
	    	Transaction transaction = new Transaction();
	    	
	    	FXMerchantDTO fxMerchantDTO = new FXMerchantDTO(100.0, CURRENCY);
	    	CustomerInfoDTO customerInfoDTO = CustomerInfoDTO.builder().email(CUSTOMER_EMAIL).build();
	    	SingleTransactionMerchantDTO singleTransactionMerchantDTO = SingleTransactionMerchantDTO.builder()
	    			.channel(CHANNEL)
	    			.build();
	    	transaction.setRefundable(true);
	    	
	    	when(fxToFXMerchantDTOConverter.convert(any())).thenReturn(fxMerchantDTO);
	    	when(customerInfoDTOConverter.convert(any())).thenReturn(customerInfoDTO);
	    	when(transactionMerchantDTOConverter.convert(any())).thenReturn(singleTransactionMerchantDTO);
	    	
	    	
	        //when
	    	SingleTransactionWrapperDTO dto = converter.convert(transaction);

	        //then
	        assertEquals(CHANNEL, dto.getTransaction().getMerchant().getChannel());
	        assertEquals(CUSTOMER_EMAIL, dto.getCustomerInfo().getEmail());
	        assertEquals(CURRENCY, dto.getFx().getMerchant().getOriginalCurrency());
	        assertEquals(MESSAGE, dto.getTransaction().getMerchant().getMessage());
	        
	        
	        
	        

	    }

}
