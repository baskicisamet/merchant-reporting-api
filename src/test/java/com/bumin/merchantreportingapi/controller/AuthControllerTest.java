package com.bumin.merchantreportingapi.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.bumin.merchantreportingapi.security.model.JWTAuthenticationResonse;
import com.bumin.merchantreportingapi.security.model.LoginRequest;
import com.bumin.merchantreportingapi.security.model.RequestStatus;
import com.bumin.merchantreportingapi.security.service.AuthService;



public class AuthControllerTest extends AbstractRestControllerTest {

	@Mock
	AuthService authService;

	@InjectMocks
	AuthController authController;

	MockMvc mockMvc;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		mockMvc = MockMvcBuilders.standaloneSetup(authController).build();
	}
	
	 
	@Test
	public void loginTest() throws Exception {
		
		//GIVEN
		LoginRequest loginRequest = new LoginRequest("test@test.com", "password");
		String mockToken = "skgemlwenglqwnglknqwlgqw.gqwgqwgw3g.w3ghwW";
		JWTAuthenticationResonse authenticationResonse = new JWTAuthenticationResonse(mockToken, RequestStatus.APPROVED);
		when(authService.authenticate(any())).thenReturn(authenticationResonse);
		
		//WHEN 
		mockMvc.perform(post("/api/v3/merchant/user/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(loginRequest)))
		//THEN
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.token", equalTo(mockToken)))
                .andExpect(jsonPath("$.status", equalTo(authenticationResonse.getStatus().toString())));
		
		
	}

}
