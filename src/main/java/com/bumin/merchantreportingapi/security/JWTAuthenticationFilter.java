package com.bumin.merchantreportingapi.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.filter.OncePerRequestFilter;

import com.bumin.merchantreportingapi.security.service.JWTUserDetailsService;
import com.bumin.merchantreportingapi.security.util.JWTValidator;


import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JWTAuthenticationFilter extends BasicAuthenticationFilter {
	
	
	public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
		super(authenticationManager);
	
	}

	@Autowired
	private JWTUserDetailsService jwtUserDetailsSercvice;
	
	@Autowired
	private JWTValidator jwtValidator;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
		
		String token = request.getHeader(JWTConstants.HEADER);
		
		if(token == null) {
			log.error("There is no token");
			chain.doFilter(request, response);
			return;
		}
		
		UsernamePasswordAuthenticationToken usernamePasswordAuth = getAuthenticationToken(token);
		SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuth);
		chain.doFilter(request, response);
		
	}
	
	private UsernamePasswordAuthenticationToken getAuthenticationToken(String token) {
		
		String email = jwtValidator.validate(token,JWTConstants.SECRET);
		
		if(email == null){
			log.error("Jwt token is invalidated");
			return null;
		}
		
		UserDetails userDetails = jwtUserDetailsSercvice.loadUserByEmail(email);
		
		return new UsernamePasswordAuthenticationToken(userDetails, null,userDetails.getAuthorities());
	}							  

}
