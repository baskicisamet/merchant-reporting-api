package com.bumin.merchantreportingapi.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bumin.merchantreportingapi.domain.Transaction;
import com.bumin.merchantreportingapi.dto.TransactionReportDTO;
import com.bumin.merchantreportingapi.dto.request.TransactionReportRequestDTO;

public interface TransactionRepository extends JpaRepository<Transaction, UUID> {

	
	@Query("select "
			+ "new com.bumin.merchantreportingapi.dto.TransactionReportDTO(count(t), sum(t.merchantTransaction.fx.orignalAmount), t.merchantTransaction.fx.orginalCurrency)"
			+ " from Transaction t"
			+ " WHERE "
				+ " t.acquirer.id = :#{#param.acquirer} AND"
				+ " t.merchant.id = :#{#param.merchant} AND"
				+ " t.merchantTransaction.createdDate > :#{#param.fromDate} AND"
				+ " t.merchantTransaction.createdDate < :#{#param.toDate}"
			+ " group by t.merchantTransaction.fx.orginalCurrency")
	List<TransactionReportDTO> getTransactionReport(@Param("param") TransactionReportRequestDTO param);
	
	
	
		
	
}
