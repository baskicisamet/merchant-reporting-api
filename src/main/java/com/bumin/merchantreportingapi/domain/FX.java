package com.bumin.merchantreportingapi.domain;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FX {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Double orignalAmount;
	
	@Enumerated(EnumType.STRING)
	private Currency orginalCurrency;
	
	
	public FX(Double orignalAmount, Currency orginalCurrency) {
		this.orignalAmount = orignalAmount;
		this.orginalCurrency = orginalCurrency;
	}
	
	
}
