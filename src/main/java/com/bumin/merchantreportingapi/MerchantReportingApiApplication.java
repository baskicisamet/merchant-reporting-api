package com.bumin.merchantreportingapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class MerchantReportingApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MerchantReportingApiApplication.class, args);
	}
	
	
	@GetMapping("/hello")
	public String hello() {
		return "hello";
	}
	
	
	
}
