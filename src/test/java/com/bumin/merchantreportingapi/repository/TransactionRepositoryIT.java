package com.bumin.merchantreportingapi.repository;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.bumin.merchantreportingapi.domain.Currency;
import com.bumin.merchantreportingapi.domain.Transaction;
import com.bumin.merchantreportingapi.dto.TransactionReportDTO;
import com.bumin.merchantreportingapi.dto.request.TransactionReportRequestDTO;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TransactionRepositoryIT extends AbstractRepositoryTest {
	

	@Autowired
	TransactionRepository transactionRepository;
	
	
	@Autowired
	@Override
	public void setTestEntityManager(TestEntityManager testEntityManager) {
		super.setTestEntityManager(testEntityManager);
	}
	
	
	
	
	public TransactionRepositoryIT() {
		
	}

	@Test
	public void findAll() throws Exception {
		
		//given
		List<Transaction> persistTransactions = new ArrayList<>();
		persistTransactions.add(new Transaction());
		persistTransactions.add(new Transaction());
		
		for(Transaction transaction : persistTransactions) {
			testEntityManager.persist(transaction);
		}
		
		//when
		List<Transaction> resultTransactions = transactionRepository.findAll();
		//then
		assertEquals("record count does not match", 2,resultTransactions.size());
	}
	
	@Test
	public void findById() throws Exception {
		
		//given
		Transaction transaction = new Transaction();
		testEntityManager.persist(transaction);
		testEntityManager.flush();
		UUID persistId = transaction.getId();
		
		//when
		UUID resultId =  transactionRepository.findById(persistId).get().getId();
		
		//then
		assertEquals("does noe match id", persistId,resultId);
		
		
		
	}
	
	
	@Test
	public void getTransactionReport() throws Exception {
		
		//given
		List<Transaction> persistentTransactions =  loadTestData();
		TransactionReportRequestDTO queryModel = new TransactionReportRequestDTO(getDate("2010-10-14"), getDate("2018-10-14"), 1l, 1l);
		int expectedReportCount = 2;
		Double[] expectedTRYTotalAmount = new Double[1];
		expectedTRYTotalAmount[0] = new Double(0);
		
		persistentTransactions.stream()
		.filter((Transaction t) -> t.getMerchantTransaction().getFx().getOrginalCurrency() == Currency.TRY)
		.forEach((Transaction t) -> {
			expectedTRYTotalAmount[0] += t.getMerchantTransaction()
					.getFx()
					.getOrignalAmount();
		});
		
		
		//when
		List<TransactionReportDTO> resultTransactions = transactionRepository.getTransactionReport(queryModel);
		Double[] resultTRYTotalAmount = new Double[1];
		resultTRYTotalAmount[0] = new Double(0);
		resultTransactions.stream()
		.filter((TransactionReportDTO t) -> t.getCurrency() == Currency.TRY)
		.forEach((TransactionReportDTO t) -> {
			resultTRYTotalAmount[0] += t.getTotal();
		});
		
		
		//then
		assertEquals("record size does not match ", expectedReportCount,resultTransactions.size());
		assertEquals("record size does not match ", expectedTRYTotalAmount[0],resultTRYTotalAmount[0]);
		
		
		
	}
	
	

}
