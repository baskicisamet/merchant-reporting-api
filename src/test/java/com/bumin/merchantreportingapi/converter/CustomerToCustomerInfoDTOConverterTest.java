package com.bumin.merchantreportingapi.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import com.bumin.merchantreportingapi.domain.Address;
import com.bumin.merchantreportingapi.domain.City;
import com.bumin.merchantreportingapi.domain.Country;
import com.bumin.merchantreportingapi.domain.Customer;
import com.bumin.merchantreportingapi.domain.State;
import com.bumin.merchantreportingapi.dto.CustomerInfoDTO;

public class CustomerToCustomerInfoDTOConverterTest {
	
	
	
    public static final Long ID_VALUE = new Long(1L);
    public static final String CUSTOMER_EMAIL  = "customer@customer.com";
    public static final String BILLING_NAME  = "samet";
    public static final String SHIPPING_NAME  = "sam";
    
    
    CustomerToCustomerInfoDTOConverter converter;
    
    @Before
    public void setUp() throws Exception {
    	converter = new CustomerToCustomerInfoDTOConverter();
    }
    
    
    @Test
    public void testNullObject() throws Exception {
        assertNull(converter.convert(null));
    }
    
    @Test
    public void convert() throws Exception {
    	
        //given
        Customer customer = new Customer();
        customer.setId(ID_VALUE);
        customer.setEmail(CUSTOMER_EMAIL);
        Address billingAddress = new Address();
        billingAddress.setFirstName(BILLING_NAME);
        billingAddress.setCity(new City());
        billingAddress.setState(new State());
        billingAddress.setCountry(new Country());
        Address shippingAddress = new Address();
        shippingAddress.setFirstName(SHIPPING_NAME);
        shippingAddress.setCity(new City());
        shippingAddress.setState(new State());
        shippingAddress.setCountry(new Country());
        customer.setBillingAddress(billingAddress);
        customer.setShippingAddress(shippingAddress);
        
        //when
        CustomerInfoDTO dto = converter.convert(customer);

        //then
        assertEquals(ID_VALUE, dto.getId());
        assertEquals(CUSTOMER_EMAIL, dto.getEmail());
        assertEquals(BILLING_NAME, dto.getBillingFirstName());
        assertEquals(SHIPPING_NAME, dto.getShippingFirstName());
        

    }

}
