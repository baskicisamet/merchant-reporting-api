package com.bumin.merchantreportingapi.security.model;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.bumin.merchantreportingapi.domain.Merchant;

public class JWTUserDetails implements UserDetails {
	
	
	
	private Long id;
	private String email;
	private String password;
	private boolean enabled;
	private Collection<? extends GrantedAuthority> authorities;
	
	

	public JWTUserDetails(Long id, String email, String password, boolean enabled,
			Collection<? extends GrantedAuthority> authorities) {
		super();
		this.id = id;
		this.email = email;
		this.password = password;
		this.enabled = enabled;
		this.authorities = authorities;
	}
	
	
	public static JWTUserDetails create(Merchant merchant) {
		
		List<GrantedAuthority> authorities = merchant.getRoles().stream()
				.map(role -> new SimpleGrantedAuthority(role.getValue().toString()))
				.collect(Collectors.toList());
		
		
					
		return new JWTUserDetails(merchant.getId(), merchant.getEmail(), merchant.getPassword(), merchant.isEnabled(), authorities);
	}
	

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.authorities;
	}

	@Override
	public String getPassword() {
		return this.password;
	}

	@Override
	public String getUsername() {
		return null;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		
		return this.enabled;
	}
	
	public Long getId() {
		return this.id;
	}
	
	public String getEmail() {
		return this.email;
	}

}
