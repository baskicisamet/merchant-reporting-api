package com.bumin.merchantreportingapi.util;

import java.math.BigInteger;
import java.util.UUID;

import com.bumin.merchantreportingapi.exception.UUIDFormatException;

import lombok.extern.slf4j.Slf4j;
@Slf4j
public class UUIDUtil {
	
	public static UUID fromString(String str) {
		str = str.replace("-", "");
		UUID uuid = null;
		try {
			uuid = new UUID(
			        new BigInteger(str.substring(0, 16), 16).longValue(),
			        new BigInteger(str.substring(16), 16).longValue());
		}catch (IndexOutOfBoundsException e) {
			log.error("invaldated UUID format");
			throw new UUIDFormatException("invaldated UUID format");
		}
		
		
		return uuid;
	}

}
