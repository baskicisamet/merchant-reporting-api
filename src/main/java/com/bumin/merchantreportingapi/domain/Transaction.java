package com.bumin.merchantreportingapi.domain;

import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Transaction {
	
	@Id
	@GeneratedValue
	private UUID id;
	
	@ManyToOne
	private Merchant merchant;
	
	@ManyToOne
	private Acquirer acquirer;
	
	@ManyToOne
	private Customer customer;
	
	@OneToOne(cascade = CascadeType.ALL)
	private MerchantTransaction merchantTransaction;
	
	@OneToOne(cascade = CascadeType.ALL)
	private AcquirerTransaction acquirerTransaction;
	
	private Boolean refundable;
	
	
	

}
