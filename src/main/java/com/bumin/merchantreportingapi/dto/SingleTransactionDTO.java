package com.bumin.merchantreportingapi.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class SingleTransactionDTO {

	private SingleTransactionMerchantDTO merchant;
}
