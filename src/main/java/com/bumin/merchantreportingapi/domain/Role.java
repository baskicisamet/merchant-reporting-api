package com.bumin.merchantreportingapi.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
@Getter
@Setter
@Entity
public class Role {
	
	
	
	public Role(RoleValue value, List<Merchant> merchants) {
		super();
		this.value = value;
		this.merchants = merchants;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Enumerated(EnumType.STRING)
	@Column(name="authority_value", length=50)
	private RoleValue value;
	
	@ManyToMany(mappedBy ="roles", fetch = FetchType.LAZY)
	private List<Merchant> merchants;

}
