package com.bumin.merchantreportingapi.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import com.bumin.merchantreportingapi.domain.Customer;
import com.bumin.merchantreportingapi.dto.CustomerInfoDTO;

import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CustomerToCustomerInfoDTOConverter implements Converter<Customer, CustomerInfoDTO> {
	
	
	@Synchronized
	@Nullable
	@Override
	public CustomerInfoDTO convert(Customer source) {
		
		if(source == null) {
			log.error("source is null");
			return  null;
		}
		
		return CustomerInfoDTO.builder()
				
				.id(source.getId())
				.createdAt(source.getCreatedDate())
				.updatedAt(source.getUpdatedDate())
				.deletedAt(source.getDeletedDate())
				.number(source.getNumber())
				.expiryMonth(toStringIfNotNull(source.getExpiryMonth()))
				.expiryYear(toStringIfNotNull(source.getExpiryYear()))
				.startMonth(toStringIfNotNull(source.getStartMonth()))
				.issueNumber(source.getIssueNumber())
				.email(source.getEmail())
				.birthDay(source.getBirthDate())
				.gender(source.getGender())
				.billingTitle(source.getBillingAddress().getTitle())
				.billingFirstName(source.getBillingAddress().getFirstName())
				.billingLastName(source.getBillingAddress().getLastName())
				.billingCompany(source.getBillingAddress().getCompany())
				.billingAddress1(source.getBillingAddress().getAddress1())
				.billingAddress2(source.getBillingAddress().getAddress2())
				.billingCity(source.getBillingAddress().getCity().getName())
				.billingPostcode(source.getBillingAddress().getPostCode())
				.billingState(source.getBillingAddress().getState().getName())
				.billingCountry(source.getBillingAddress().getCountry().getName())
				.billingPhone(source.getBillingAddress().getPhone())
				.billingFax(source.getBillingAddress().getFax())
				
				.shippingTitle(source.getShippingAddress().getTitle())
				.shippingFirstName(source.getShippingAddress().getFirstName())
				.shippingLastName(source.getShippingAddress().getLastName())
				.shippingCompany(source.getShippingAddress().getCompany())
				.shippingAddress1(source.getShippingAddress().getAddress1())
				.shippingAddress2(source.getShippingAddress().getAddress2())
				.shippingCity(source.getShippingAddress().getCity().getName())
				.shippingPostcode(source.getShippingAddress().getPostCode())
				.shippingState(source.getShippingAddress().getState().getName())
				.shippingCountry(source.getShippingAddress().getCountry().getName())
				.shippingPhone(source.getShippingAddress().getPhone())
				.shippingFax(source.getShippingAddress().getFax())
				
				.build();
			
			
	}
	
	private String toStringIfNotNull(Object object) {
		if(object == null) {
			return null;
		}
		return object.toString();
	}

}
