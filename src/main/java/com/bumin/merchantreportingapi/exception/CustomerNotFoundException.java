package com.bumin.merchantreportingapi.exception;

public class CustomerNotFoundException  extends RuntimeException {
	

	public CustomerNotFoundException() {
		
	}
	
	public CustomerNotFoundException(String mesage) {
		super(mesage);
	}
	
	public CustomerNotFoundException(String message,Throwable cause) {
		super(message,cause);
	}
	
	public CustomerNotFoundException(Throwable cause) {
		super(cause);
	}
	
	public CustomerNotFoundException(String message,Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message,cause,enableSuppression,writableStackTrace);
	}

}