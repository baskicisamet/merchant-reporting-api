package com.bumin.merchantreportingapi.security.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.bumin.merchantreportingapi.domain.Merchant;
import com.bumin.merchantreportingapi.repository.MerchantRepository;
import com.bumin.merchantreportingapi.security.model.JWTUserDetails;
import com.bumin.merchantreportingapi.security.service.JWTUserDetailsService;

public class JWTUserDetailsServiceTest {

	JWTUserDetailsService jwtUserDetailsService;

	@Mock
	MerchantRepository merchantRepository;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		jwtUserDetailsService = new JWTUserDetailsService();
		jwtUserDetailsService.setMerchantRepository(merchantRepository);
	}

	@Test
	public void loadUserByEmailTest() throws Exception {

		// GIVEN
		String email = "test@test.com";
		Merchant merchant = new Merchant();
		merchant.setId(1l);
		merchant.setName("test");
		merchant.setPassword("password");
		merchant.setEmail(email);
		Optional<Merchant> merchantOptional = Optional.of(merchant);
		when(merchantRepository.findByEmail(anyString())).thenReturn(merchantOptional);

		// WHEN
		JWTUserDetails jwrUserDetails = (JWTUserDetails) jwtUserDetailsService.loadUserByEmail(email);

		// THEN
		assertEquals("does not match email fields", jwrUserDetails.getEmail(), email);
		assertEquals("does not match id fields", jwrUserDetails.getId(), (Long) 1l);
		assertEquals("does not match password fields", jwrUserDetails.getPassword(), "password");
		verify(merchantRepository, times(1)).findByEmail(anyString());
		verify(merchantRepository, never()).findById(anyLong());

	}

}
