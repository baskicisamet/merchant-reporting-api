package com.bumin.merchantreportingapi.controller;

import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.bumin.merchantreportingapi.exception.ExceptionResult;
import com.bumin.merchantreportingapi.repository.MerchantRepository;
import com.bumin.merchantreportingapi.security.model.JWTAuthenticationResonse;
import com.bumin.merchantreportingapi.security.model.JWTUserDetails;
import com.bumin.merchantreportingapi.security.model.LoginRequest;
import com.bumin.merchantreportingapi.security.model.RequestStatus;
import com.bumin.merchantreportingapi.security.service.AuthService;
import com.bumin.merchantreportingapi.security.util.JWTGenerator;

import io.jsonwebtoken.JwtException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("api/v3/merchant")
public class AuthController {

	
	private AuthService authService;
	
	
	@Autowired
	public AuthController(AuthService authService) {
		super();
		this.authService = authService;
	}




	@PostMapping("/user/login")
	public JWTAuthenticationResonse login(@Valid @RequestBody LoginRequest loginRequest) {

		return authService.authenticate(loginRequest);
		

	}
	

	
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(JwtException.class)
    public ExceptionResult handleNotFound(JwtException ex){
 
		return new ExceptionResult(new Date(), ex.getClass().toString(), ex.getMessage());
        
       
    }
	
	
}
