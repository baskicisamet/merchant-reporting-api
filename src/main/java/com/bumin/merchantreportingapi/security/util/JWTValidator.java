package com.bumin.merchantreportingapi.security.util;

import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;

@Component
public class JWTValidator {
	
	
	public String validate(String token, String secretKey) {//it returns rmail if validate successfull
		
		try {
			Claims claims = getClaims(token, secretKey);
			return claims.getSubject();
		}catch (JwtException e) {
			return null;
		}
			
		
		
	}
	
	
	private Claims getClaims(String token, String secretKey) {
	
		return Jwts.parser()
		        .setSigningKey(secretKey)
		        .parseClaimsJws(token)
		        .getBody();
		
	}

}
