package com.bumin.merchantreportingapi.domain;

public enum TransactionPaymentMethod {
	CREDITCARD, CUP, IDEAL, GIROPAY, MISTERCASH, STORED, PAYTOCARD, CEPBANK, CITADEL
}
