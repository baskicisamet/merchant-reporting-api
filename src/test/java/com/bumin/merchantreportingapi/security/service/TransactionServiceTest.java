package com.bumin.merchantreportingapi.security.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.bumin.merchantreportingapi.converter.TransactionToSingleTransactionWrapperDTOConverter;
import com.bumin.merchantreportingapi.domain.Currency;
import com.bumin.merchantreportingapi.domain.Transaction;
import com.bumin.merchantreportingapi.dto.TransactionReportDTO;
import com.bumin.merchantreportingapi.exception.TransactionReportEmptyContentException;
import com.bumin.merchantreportingapi.repository.TransactionRepository;
import com.bumin.merchantreportingapi.service.TransactionService;
import com.bumin.merchantreportingapi.service.TransactionServiceImpl;

public class TransactionServiceTest {
	
	
	TransactionService  transactionService;

	@Mock
	TransactionRepository transactionRepository;
	
	@Mock
	TransactionToSingleTransactionWrapperDTOConverter converter;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		transactionService = new TransactionServiceImpl(transactionRepository,converter);
	}
	
	
	@Test
	public void getTransactionReports() throws Exception {
		
		
		//given
		List<TransactionReportDTO> givenReports = new ArrayList<>();
		givenReports.add(new TransactionReportDTO(3l, 100.0, Currency.TRY));
		givenReports.add(new TransactionReportDTO(5l, 200.0, Currency.EUR));
		givenReports.add(new TransactionReportDTO(3l, 150.0, Currency.USD));
		
		when(transactionRepository.getTransactionReport(any())).thenReturn(givenReports);
		
		//when
		List<TransactionReportDTO> resultReports = transactionService.getReports(any()).getResponse();
		
		//then
		assertEquals("reports count does not match", givenReports.size(),resultReports.size());
		assertEquals("amount does not match", givenReports.get(0),resultReports.get(0));
		verify(transactionRepository, times(1)).getTransactionReport(any());
		
		
	}
	
	@Test(expected = TransactionReportEmptyContentException.class) 
	public void getEmptyTransactionReport() throws Exception {
		
		//given
		List<TransactionReportDTO> givenReports = new ArrayList<>();
		
		when(transactionRepository.getTransactionReport(any())).thenReturn(givenReports);
		
		//when
		transactionService.getReports(any());
		
	}
	
	
	
	
	
	

}
