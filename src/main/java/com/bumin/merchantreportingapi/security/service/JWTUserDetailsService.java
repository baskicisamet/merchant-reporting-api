package com.bumin.merchantreportingapi.security.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.bumin.merchantreportingapi.domain.Merchant;
import com.bumin.merchantreportingapi.exception.MerchantNotFoundException;
import com.bumin.merchantreportingapi.repository.MerchantRepository;
import com.bumin.merchantreportingapi.security.model.JWTUserDetails;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@Service
public class JWTUserDetailsService implements UserDetailsService {

	@Autowired
	private MerchantRepository merchantRepository;
	
	
	@Override
	public UserDetails loadUserByUsername(String usernameOrEmail) throws UsernameNotFoundException {
		
		return loadUserByEmail(usernameOrEmail);
		
	}
	
	
	public UserDetails loadUserByEmail(String email) {
		
		Optional<Merchant> merchant = merchantRepository.findByEmail(email);
		if(!merchant.isPresent()) {
			log.error("Merchant not found with this email address");
			throw new MerchantNotFoundException("Merchant not found with this email address");
		}
		return merchant.map(JWTUserDetails::create).get();
		
	}
	

}
