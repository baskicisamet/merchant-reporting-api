package com.bumin.merchantreportingapi.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Acquirer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	@Column(length =2)
	private String code;
	
	@Enumerated(EnumType.STRING)
	private TransactionPaymentMethod type;
	
	@OneToMany( mappedBy = "acquirer")
	private List<Transaction> transactions = new ArrayList<>();

	public Acquirer addTransaction(Transaction transaction) {
		transactions.add(transaction);
		transaction.setAcquirer(this);
		return this;
	}
	
	
	
	
	
	

}
