package com.bumin.merchantreportingapi.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Address {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String title;
	private String firstName;
	private String lastName;
	private String company;
	private String address1;
	private String address2;
	private String postCode;
	private String phone;
	private String fax;
	
	@ManyToOne
	private Country country;
	
	@ManyToOne
	private State state;
	
	@ManyToOne
	private City city;
	
	
}
