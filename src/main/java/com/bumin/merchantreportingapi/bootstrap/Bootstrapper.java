package com.bumin.merchantreportingapi.bootstrap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.bumin.merchantreportingapi.domain.Acquirer;
import com.bumin.merchantreportingapi.domain.AcquirerTransaction;
import com.bumin.merchantreportingapi.domain.Address;
import com.bumin.merchantreportingapi.domain.City;
import com.bumin.merchantreportingapi.domain.Country;
import com.bumin.merchantreportingapi.domain.Currency;
import com.bumin.merchantreportingapi.domain.Customer;
import com.bumin.merchantreportingapi.domain.FX;
import com.bumin.merchantreportingapi.domain.Merchant;
import com.bumin.merchantreportingapi.domain.MerchantTransaction;
import com.bumin.merchantreportingapi.domain.Role;
import com.bumin.merchantreportingapi.domain.RoleValue;
import com.bumin.merchantreportingapi.domain.State;
import com.bumin.merchantreportingapi.domain.Transaction;
import com.bumin.merchantreportingapi.domain.TransactionErrorCode;
import com.bumin.merchantreportingapi.domain.TransactionOperation;
import com.bumin.merchantreportingapi.domain.TransactionPaymentMethod;
import com.bumin.merchantreportingapi.repository.AcquirerRepository;
import com.bumin.merchantreportingapi.repository.CityRepository;
import com.bumin.merchantreportingapi.repository.CountryRepository;
import com.bumin.merchantreportingapi.repository.CustomerRepository;
import com.bumin.merchantreportingapi.repository.MerchantRepository;
import com.bumin.merchantreportingapi.repository.RoleRepository;
import com.bumin.merchantreportingapi.repository.StateRepository;
import com.bumin.merchantreportingapi.repository.TransactionErrorCodeRepository;
import com.bumin.merchantreportingapi.repository.TransactionRepository;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@Component
public class Bootstrapper implements CommandLineRunner {

	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private  BCryptPasswordEncoder passwordEncoder;
	@Autowired
	private CountryRepository countryRepository;
	@Autowired
	private StateRepository stateRepository;
	@Autowired
	private CityRepository cityRepository;
	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private TransactionErrorCodeRepository transactionErrorCodeRepository;
	@Autowired
	private MerchantRepository merchantRepository;
	@Autowired
	private AcquirerRepository acquirerRepository;
	@Autowired
	private TransactionRepository transactionRepository;
	
	
	
	private Map<String,Country> countryMap;
	private Map<String, State> stateMap;
	private List<City>  cities;
	private List<Customer> customers;
	private List<Merchant> merchants;
	private List<Role> roles;
	private List<Acquirer> acquirers;
	private List<TransactionErrorCode> transactionErrorCodes;
	private List<Transaction> transactions;
	


	@Override
	public void run(String... args) throws Exception {
		
		initCountry();
		initState();
		initCity();
		initCustomer();
		initTransactionErrorCode();
		initRole();
		initMerchant();
		initAcquirer();
		initTransactions();
		
		countryRepository.saveAll(countryMap.values());
		stateRepository.saveAll(stateMap.values());
		cityRepository.saveAll(cities);
		customerRepository.saveAll(customers);
		transactionErrorCodeRepository.saveAll(transactionErrorCodes);
		roleRepository.saveAll(roles);
		merchantRepository.saveAll(merchants);
		acquirerRepository.saveAll(acquirers);
		transactionRepository.saveAll(transactions);
		
		log.info("SAVED TRANSACTION ID FOR TEST :::: \"" + transactions.get(0).getId() + "\"");
		
	}
	
	
	
	
	

	private Map<String,Country> initCountry() {
		
		countryMap = new HashMap<>();
		countryMap.put("TR", new Country("TR"));
		countryMap.put("US", new Country("US"));
		countryMap.put("DE", new Country("DE"));
		
		return countryMap;
	}
	
	
	
	
	private Map<String,State> initState(){
		
		stateMap = new HashMap<>();
		
		stateMap.put("stateTR1",new State("stateTR1", this.countryMap.get("TR")));
		
		stateMap.put("stateUS1",new State("stateUS1", this.countryMap.get("US")));
		stateMap.put("stateUS2",new State("stateUS2", this.countryMap.get("US")));
		
		stateMap.put("stateDE1",new State("stateDE1", this.countryMap.get("DE")));
		stateMap.put("stateDE2",new State("stateDE2", this.countryMap.get("DE")));
		
		return stateMap;
	}
	
	
	
	
	private List<City> initCity(){
		
		cities = new ArrayList<>();
		
		cities.add(new City("İzmir", stateMap.get("stateTR1")));
		cities.add(new City("İstanbul", stateMap.get("stateTR1")));
		cities.add(new City("Ankara", stateMap.get("stateTR1")));
		cities.add(new City("Eskişehir", stateMap.get("stateTR1")));
		cities.add(new City("Antalya", stateMap.get("stateTR1")));
		
		
		cities.add(new City("New York", stateMap.get("stateUS1")));
		cities.add(new City("Los Angeles", stateMap.get("stateUS1")));
		cities.add(new City("Chicago", stateMap.get("stateUS1")));
		cities.add(new City("San Diego", stateMap.get("stateUS1")));
		cities.add(new City("Dallas", stateMap.get("stateUS1")));
		
		cities.add(new City("Berlin", stateMap.get("stateDE1")));
		cities.add(new City("Hamburg", stateMap.get("stateDE1")));
		cities.add(new City("Munich", stateMap.get("stateDE1")));
		cities.add(new City("Stuttgart", stateMap.get("stateDE1")));
		cities.add(new City("Düsseldorf", stateMap.get("stateDE1")));
		
		
		return cities;
	}
	
	
	private List<Customer> initCustomer(){
		
		customers = new ArrayList<>();
		
		
		for(int i=0;i<10;i++) {
			
			Address shippingAddress = new Address();
			shippingAddress.setAddress1("Address1");
			shippingAddress.setAddress2("address2");
			shippingAddress.setCity(cities.get( i % cities.size()));
			shippingAddress.setState(cities.get(i % cities.size()).getState());
			shippingAddress.setCountry(cities.get(i % cities.size()).getState().getCountry());
			shippingAddress.setFax("09237958");
			shippingAddress.setFirstName("firsname1");
			shippingAddress.setLastName("lastname1");
			shippingAddress.setCompany("company");
			shippingAddress.setPhone("0289453443");
			shippingAddress.setPostCode("45000");
			shippingAddress.setTitle("title");
			
			Address billingAddress = new Address();
			billingAddress.setAddress1("Address1");
			billingAddress.setAddress2("address2");
			billingAddress.setCity(cities.get( i % cities.size()));
			billingAddress.setState(cities.get(i % cities.size()).getState());
			billingAddress.setCountry(cities.get(i % cities.size()).getState().getCountry());
			billingAddress.setFax("09237958");
			billingAddress.setFirstName("firsname1");
			billingAddress.setLastName("lastname1");
			billingAddress.setCompany("company");
			billingAddress.setPhone("0289453443");
			billingAddress.setPostCode("45000");
			billingAddress.setTitle("title");
			
			Customer customer = new Customer();
			customer.setCreatedDate(new Date());
			customer.setUpdatedDate(new Date());
			customer.setDeletedDate(new Date());
			customer.setNumber("34654");
			customer.setExpiryMonth((short) 6);
			customer.setExpiryYear((short) 2019);
			customer.setEmail("customer " + i + "@customer.com");
			customer.setBirthDate(new Date());
			customer.setShippingAddress(shippingAddress);
			customer.setBillingAddress(billingAddress);
			
			customers.add(customer);
			
			
		}
		
		
		return customers;
	}
	
	
	
	private List<Role> initRole(){
		
		
		roles =  Arrays.asList(RoleValue.values()).stream()
					.map(roleValue -> {
						Role role = new Role(); 
						role.setValue(roleValue);
						return role;
					})
					.collect(Collectors.toList());
		
		return roles;
		
		
	}
	
	private List<Merchant> initMerchant(){
		
		merchants = new ArrayList<>();
		
		for(int i=0;i<10;i++) {
			Merchant merchant = new Merchant();
			merchant.setName("Merchant" + i);
			merchant.setEmail("merchant" + i + "@merchant.com");
			merchant.setPassword(passwordEncoder.encode("password"));
			merchant.addRole(roles.get(i % roles.size()));
			merchant.setEnabled(true);
			
			merchants.add(merchant);
		}
		
		return merchants;
	}
	
private List<Acquirer> initAcquirer() {
		
		acquirers = new ArrayList<>();
		
		for(int i=0;i<10;i++) {
			Acquirer acquirer = new Acquirer();
			acquirer.setCode("XX");
			acquirer.setName("Bank" + i);
			acquirer.setType(TransactionPaymentMethod.values()[i % TransactionPaymentMethod.values().length]);
			
			acquirers.add(acquirer);
		}
		
		return acquirers;
	}
	
	
	private List<TransactionErrorCode> initTransactionErrorCode() {
		
		transactionErrorCodes = new ArrayList<>();
		transactionErrorCodes.add(new TransactionErrorCode("Do not honor"));
		transactionErrorCodes.add(new TransactionErrorCode("Invalid Transaction"));
		transactionErrorCodes.add(new TransactionErrorCode("Invalid Card"));
		transactionErrorCodes.add(new TransactionErrorCode("Not sufficient funds"));
		transactionErrorCodes.add(new TransactionErrorCode("Incorrect PIN"));
		transactionErrorCodes.add(new TransactionErrorCode("Invalid country association"));
		transactionErrorCodes.add(new TransactionErrorCode("Currency not allowed"));
		transactionErrorCodes.add(new TransactionErrorCode("3-D Secure Transport Error"));
		transactionErrorCodes.add(new TransactionErrorCode("Transaction not permitted to cardholder"));
		
		return transactionErrorCodes;
		
	}
	
	
	private List<Transaction> initTransactions(){
		
		transactions = new ArrayList<>();
		
		for(int i=0;i<100;i++) {
			
			FX fx = new FX((double)this.getRandomNumber(0, 100), Currency.values()[this.getRandomNumber(0, Currency.values().length-1)]);
			
			MerchantTransaction merchantTransaction = new MerchantTransaction();
			merchantTransaction.setFx(fx);
			merchantTransaction.setReferenceNo("3489769");
			merchantTransaction.setMessage("message");
			merchantTransaction.setChannel("API");
			merchantTransaction.setTransactionErrorCode(transactionErrorCodes.get( i % transactionErrorCodes.size()));
			merchantTransaction.setCreatedDate(new Date());
			merchantTransaction.setUpdatedDate(new Date());
			merchantTransaction.setOperation(TransactionOperation.values()[ i % TransactionOperation.values().length]);
			
			AcquirerTransaction acquirerTransaction = new AcquirerTransaction();
			
			
			Transaction transaction = new Transaction();
			transaction.setMerchant(merchants.get(i % merchants.size()));
			transaction.setAcquirer(acquirers.get(i % acquirers.size()));
			transaction.setCustomer(customers.get(i % customers.size()));
			transaction.setMerchantTransaction(merchantTransaction);
			merchantTransaction.setTransaction(transaction);
			transaction.setAcquirerTransaction(acquirerTransaction);
			acquirerTransaction.setTransaction(transaction);
			
			transaction.setRefundable(i % 2 == 0);
			transaction.setCustomer(customers.get(i % customers.size()));
			
			transactions.add(transaction);
			
		}
			
		return transactions;
		
	}
	
	
	
	
	private static int getRandomNumber(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}


}
