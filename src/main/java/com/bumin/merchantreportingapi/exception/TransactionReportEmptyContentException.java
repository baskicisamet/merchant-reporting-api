package com.bumin.merchantreportingapi.exception;

public class TransactionReportEmptyContentException  extends RuntimeException {
	

	public TransactionReportEmptyContentException() {
		
	}
	
	public TransactionReportEmptyContentException(String mesage) {
		super(mesage);
	}
	
	public TransactionReportEmptyContentException(String message,Throwable cause) {
		super(message,cause);
	}
	
	public TransactionReportEmptyContentException(Throwable cause) {
		super(cause);
	}
	
	public TransactionReportEmptyContentException(String message,Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message,cause,enableSuppression,writableStackTrace);
	}

}

