package com.bumin.merchantreportingapi.dto;

import java.util.List;

import com.bumin.merchantreportingapi.domain.TransactionStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TransactionReportsWrapperDTO {
	
	private TransactionStatus status;
	private List<TransactionReportDTO> response;
	

}
