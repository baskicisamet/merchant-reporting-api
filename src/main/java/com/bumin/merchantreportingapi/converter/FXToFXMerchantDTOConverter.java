package com.bumin.merchantreportingapi.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import com.bumin.merchantreportingapi.domain.FX;
import com.bumin.merchantreportingapi.dto.FXMerchantDTO;

import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Component
public class FXToFXMerchantDTOConverter implements Converter<FX, FXMerchantDTO> {

	
	@Synchronized
	@Nullable
	@Override
	public FXMerchantDTO convert(FX source) {
		
		if(source == null) {
			log.error("source is null");
			return null;
		}
		
		FXMerchantDTO  target = new FXMerchantDTO();
		target.setOriginalCurrency(source.getOrginalCurrency());
		target.setOrignalAmount(source.getOrignalAmount());
		return target;
	}

}
