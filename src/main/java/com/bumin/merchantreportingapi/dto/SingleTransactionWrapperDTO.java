package com.bumin.merchantreportingapi.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SingleTransactionWrapperDTO {
	
	private FXDTO fx;
	private CustomerInfoDTO customerInfo;
	private SingleTransactionMerchantInfoDTO merchant;
	private SingleTransactionDTO transaction;

}
