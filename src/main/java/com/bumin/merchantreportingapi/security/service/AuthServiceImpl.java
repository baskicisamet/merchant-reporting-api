package com.bumin.merchantreportingapi.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.bumin.merchantreportingapi.repository.MerchantRepository;
import com.bumin.merchantreportingapi.security.model.JWTAuthenticationResonse;
import com.bumin.merchantreportingapi.security.model.JWTUserDetails;
import com.bumin.merchantreportingapi.security.model.LoginRequest;
import com.bumin.merchantreportingapi.security.model.RequestStatus;
import com.bumin.merchantreportingapi.security.util.JWTGenerator;

@Service
public class AuthServiceImpl implements AuthService {

	
	private AuthenticationManager authenticationManager;
	private JWTGenerator jwtGenerator;
	
	
	@Autowired
	public AuthServiceImpl(AuthenticationManager authenticationManager, JWTGenerator jwtGenerator){
		super();
		this.authenticationManager = authenticationManager;
		this.jwtGenerator = jwtGenerator;
	}



	@Override
	public JWTAuthenticationResonse authenticate(LoginRequest loginRequest) {
		
		Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getEmail(),
                        loginRequest.getPassword()
                )
        );
		
		String token = jwtGenerator.generate((JWTUserDetails)authentication.getPrincipal());
		return  new JWTAuthenticationResonse(token, RequestStatus.APPROVED);
		
	}

}
