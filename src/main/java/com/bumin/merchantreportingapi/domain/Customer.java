package com.bumin.merchantreportingapi.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Customer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date deletedDate;
	
	private String number;
	
	private Short expiryMonth;
	private Short expiryYear;
	private Short startMonth;
	private Short startYear;
	private String issueNumber;
	
	@Column(unique = true)
	private String email;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date birthDate;
	
	private String gender;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Address shippingAddress;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Address billingAddress;
	
	@OneToMany( mappedBy = "customer")
	private List<Transaction> transactions = new ArrayList<>();
	
	
	
	
	
	
	
	
	

}
