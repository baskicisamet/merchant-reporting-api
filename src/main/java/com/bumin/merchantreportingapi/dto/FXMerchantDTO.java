package com.bumin.merchantreportingapi.dto;

import com.bumin.merchantreportingapi.domain.Currency;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class FXMerchantDTO {
	
	private Double orignalAmount;
	private Currency originalCurrency;
	

}
