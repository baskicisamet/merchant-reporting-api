package com.bumin.merchantreportingapi.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import com.bumin.merchantreportingapi.domain.Currency;
import com.bumin.merchantreportingapi.domain.FX;
import com.bumin.merchantreportingapi.dto.FXMerchantDTO;

public class FXToFXMerchantDTOConverterTest {
	
	
    
    public static final Currency ORIGINAL_CURRENCY  = Currency.TRY;
    public static final Double ORIGINAL_AMOUNT= 500.0;
    
   
    
    FXToFXMerchantDTOConverter converter;
    
    @Before
    public void setUp() throws Exception {
    	converter = new FXToFXMerchantDTOConverter(); 
    }
    
    
    @Test
    public void testNullObject() throws Exception {
        assertNull(converter.convert(null));
    }
    
    @Test
    public void convert() throws Exception {
    	
        //given
    	FX fx = new FX(ORIGINAL_AMOUNT, ORIGINAL_CURRENCY);
    	
        
        //when
        FXMerchantDTO dto = converter.convert(fx);

        //then
        
        assertEquals(ORIGINAL_CURRENCY, dto.getOriginalCurrency());
        assertEquals(ORIGINAL_AMOUNT, dto.getOrignalAmount());
        
        

    }


}
