package com.bumin.merchantreportingapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bumin.merchantreportingapi.domain.State;

public interface StateRepository extends JpaRepository<State, Long>{
	
}
