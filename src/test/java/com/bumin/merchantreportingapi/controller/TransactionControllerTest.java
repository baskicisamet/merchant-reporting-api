package com.bumin.merchantreportingapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.bumin.merchantreportingapi.domain.Currency;
import com.bumin.merchantreportingapi.domain.TransactionStatus;
import com.bumin.merchantreportingapi.dto.TransactionReportDTO;
import com.bumin.merchantreportingapi.dto.TransactionReportsWrapperDTO;
import com.bumin.merchantreportingapi.dto.request.TransactionReportRequestDTO;
import com.bumin.merchantreportingapi.service.TransactionService;

public class TransactionControllerTest extends AbstractRestControllerTest {

	@Mock
	TransactionService transactionService;

	TransactionController controller;

	MockMvc mockMvc;

	@Before
	public void setUp() throws Exception {

		MockitoAnnotations.initMocks(this);

		controller = new TransactionController(transactionService);
		mockMvc = MockMvcBuilders.standaloneSetup(controller)
				.setControllerAdvice(new RestResponseEntityExceptionHandler()).build();

	}

	@Test
	public void getTransactionReportDTO() throws Exception {

		// given
		TransactionReportRequestDTO request = new TransactionReportRequestDTO();
		List<TransactionReportDTO> reports = new ArrayList<>();
		reports.add(new TransactionReportDTO(3l, 100.0, Currency.TRY));
		reports.add(new TransactionReportDTO(5l, 200.0, Currency.EUR));
		reports.add(new TransactionReportDTO(3l, 150.0, Currency.USD));
		TransactionReportsWrapperDTO reportsWrapper = new TransactionReportsWrapperDTO(TransactionStatus.APPROVED, reports);
		when(transactionService.getReports(any())).thenReturn(reportsWrapper);

		// when
		mockMvc.perform(post("/api/v3/transaction/report")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(request)))
		// then
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status", equalTo("APPROVED")))
				.andExpect(jsonPath("$.response", hasSize(3)));
				
				

	}
	
	

}
