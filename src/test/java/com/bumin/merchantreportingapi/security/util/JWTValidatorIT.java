package com.bumin.merchantreportingapi.security.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import com.bumin.merchantreportingapi.security.JWTConstants;
import com.bumin.merchantreportingapi.security.model.JWTUserDetails;

public class JWTValidatorIT {

	//Integration test with JWTGenerator
	
	private JWTGenerator jwtGenerator;
	private JWTValidator jwtValidator;
	
	@Before
    public void setUp() throws Exception {
		jwtGenerator = new JWTGenerator();
        jwtValidator = new JWTValidator();
    }
	
	
	@Test
	public void validateSuccessfullTest() {
		//GIVEN
		JWTUserDetails jwtUserDetails = new JWTUserDetails(1l, "xxx@xxx.com", "qqq", true, null);
		String token = jwtGenerator.generate(jwtUserDetails);
		//WHEN
		String mail = jwtValidator.validate(token, JWTConstants.SECRET);
		//THEN
		assertEquals("validation fail",mail, "xxx@xxx.com");
	}
	
	
	@Test
	public void validateFailTest() {
		//GIVEN
		String expiredToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ4eHhAeHh4LmNvbSIsImlhdCI6MTUyNDQ4ODU5MiwiZXhwIjoxNTI0NDg5MTkyfQ.aQEcCPSWou_mFVmH6OyepjDrfpwhpUkuEWv1fF_NNpxusPEXL3QFdyDCc4hnvp4HXQm-1n2hUKPMDUDB3pucLg";
		//WHEN
		String email = jwtValidator.validate(expiredToken, JWTConstants.SECRET);
		//THEN
		assertNull(" it is validate",email );
	}
}
