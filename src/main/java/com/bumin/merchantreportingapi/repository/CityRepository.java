package com.bumin.merchantreportingapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bumin.merchantreportingapi.domain.City;

public interface CityRepository extends JpaRepository<City, Long> {

}
