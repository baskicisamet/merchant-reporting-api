package com.bumin.merchantreportingapi.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.bumin.merchantreportingapi.exception.CustomerNotFoundException;
import com.bumin.merchantreportingapi.exception.MerchantNotFoundException;
import com.bumin.merchantreportingapi.exception.TransactionNotFoundException;
import com.bumin.merchantreportingapi.exception.TransactionReportEmptyContentException;
import com.bumin.merchantreportingapi.exception.UUIDFormatException;

@ControllerAdvice
public class RestResponseEntityExceptionHandler {

	
    @ExceptionHandler({MerchantNotFoundException.class})
    public ResponseEntity<Object> handleMerchnatNotFoundException(Exception exception, WebRequest request){

        return new ResponseEntity<Object>(exception.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND);

    }
    
    @ExceptionHandler({TransactionReportEmptyContentException.class})
    public ResponseEntity<Object> handleTransactionReportEmtyContentException(Exception exception, WebRequest request){

        return new ResponseEntity<Object>(exception.getMessage(), new HttpHeaders(), HttpStatus.NO_CONTENT);

    }
    
    @ExceptionHandler({TransactionNotFoundException.class})
    public ResponseEntity<Object> handleTransactionNotFoundException(Exception exception, WebRequest request){

        return new ResponseEntity<Object>(exception.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND);

    }
    
    @ExceptionHandler({CustomerNotFoundException.class})
    public ResponseEntity<Object> handleCustomerNotFoundException(Exception exception, WebRequest request){

        return new ResponseEntity<Object>(exception.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND);

    }
    
    @ExceptionHandler({UUIDFormatException.class})
    public ResponseEntity<Object> handleUUIDFormatException(Exception exception, WebRequest request){

        return new ResponseEntity<Object>(exception.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);

    }
    
	
}
