package com.bumin.merchantreportingapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bumin.merchantreportingapi.domain.Country;

public interface CountryRepository extends JpaRepository<Country, Long> {

	
}
